// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include "resource.h"
#include <math.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


// WM_CLOSE

INT_PTR CALLBACK DialogProc(
	_In_ HWND   hwndDlg,
	_In_ UINT   uMsg,
	_In_ WPARAM wParam,
	_In_ LPARAM lParam
)
{
	if (uMsg == WM_CLOSE)
	{
		// EndDialog
		EndDialog(hwndDlg, 0);
	}
	if (uMsg == WM_COMMAND)
	{
		if (wParam == IDOK)
		{
			// GetDlgItemInt
			int a = GetDlgItemInt(hwndDlg, IDC_EDIT2, 0, 1);
			int b = GetDlgItemInt(hwndDlg, IDC_EDIT1, 0, 1);
			int c = GetDlgItemInt(hwndDlg, IDC_EDIT3, 0, 1);

			float D = b*b - 4 * a*c;
			float x1 = (-b + sqrt(D)) / (2 * a);
			float x2 = (-b - sqrt(D)) / (2 * a); 

			SetDlgItemInt(hwndDlg, IDC_EDIT5, x1, 1);
			SetDlgItemInt(hwndDlg, IDC_EDIT4, x2, 1);


		}
	}

	return 0;
}

int main()
{
	DialogBox(0, L"TRIONIMO", 0,DialogProc);
    return 0;
}


